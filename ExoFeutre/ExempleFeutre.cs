﻿// See https://aka.ms/new-console-template for more information
using Feutres;

Feutre unFeutreBleu;
Feutre unAutreFeutreBleu = new("Plastique", ConsoleColor.Blue);
Feutre unFeutreRouge;
Feutre unFeutreVert;

Console.WriteLine("-----> 1");
unFeutreBleu = new Feutre();
unFeutreBleu.Debouche();
unFeutreBleu.Ecrire("Hello les bleus");

Console.WriteLine("-----> 2");
unAutreFeutreBleu.Debouche();
unAutreFeutreBleu.Ecrire("Encore en bleu...");
unAutreFeutreBleu.Bouche();
unAutreFeutreBleu.Ecrire("Tentative bouché...");

Console.WriteLine("-----> 3");
unFeutreRouge = new Feutre("Métal", ConsoleColor.Red, false);
unFeutreRouge.Ecrire("Cette fois c'est en rouge");

Console.WriteLine("-----> 4");
unFeutreVert = new Feutre("Métal", ConsoleColor.Green, true);
unFeutreVert.Ecrire("Cette fois c'est en vert");

Console.Write("\n\n\t\tUNE TOUCHE");
Console.ReadKey();
