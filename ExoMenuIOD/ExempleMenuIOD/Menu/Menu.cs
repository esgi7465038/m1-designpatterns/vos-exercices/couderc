﻿namespace Menu;

internal class Menu
{
    private readonly int nTable;
    protected List<IPlat> lesPlats;

    public Menu(int nTable, IPlat[]? lesPlats = null) 
    {
        this.nTable = nTable;
        if (lesPlats == null)
            this.lesPlats = new List<IPlat>();
        else
            this.lesPlats = new List<IPlat>(lesPlats);
    }

    public Menu AddPlat(IPlat plat) 
    {
        lesPlats.Add(plat);
        return this;
    }

    public string Consommer()
    {
        string conso = "La table " + nTable + " est servie.\n";
        foreach (IPlat le in lesPlats)
        {
            conso += le.Manger();
        }

        return conso;
    }

}
