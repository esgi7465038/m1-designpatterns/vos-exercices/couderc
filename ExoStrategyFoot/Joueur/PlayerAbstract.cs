﻿using ExoStrategyFoot.Action;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoStrategyFoot.Joueur
{
    internal abstract class PlayerAbstract
    {
        public int numero;
        public int Numero
        {
            get { return numero; }
            private set { numero = value; }
        }
        public IPlayingAction? PlayingAction { get; set; }
        
        public PlayerAbstract(int numero)
        {
            this.numero = numero;
        }

        public virtual void DoAction()
        {
            if (PlayingAction != null)
            {
                Console.Write("Le joueur {0} exécute une action : ", numero);
                PlayingAction.ActionToDo();
            }
            else
            {
                Console.WriteLine("Le joueur " + numero + " n'a pas d'action définie.");
            }
        }
    }
}
