﻿using System;
using System.Collections.Generic;
using ExoStrategyFoot.Joueur;
using ExoStrategyFoot.Action;

namespace ExoStrategyFoot
{
    internal class demoComportement
    {
        private List<PlayerAbstract> players = new List<PlayerAbstract>();

        public void AddPlayer(PlayerAbstract player)
        {
            players.Add(player);
        }

        public void Jouer()
        {
            foreach (PlayerAbstract player in players)
            {
                player.DoAction();
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            demoComportement demo = new demoComportement();

            Ailier ailier1 = new Ailier(7) { PlayingAction = new Dribble() };
            demo.AddPlayer(ailier1);
            Console.WriteLine("Création d'un ailier avec action dribble");

            Milieu milieu = new Milieu(8) { PlayingAction = new Passe() };
            demo.AddPlayer(milieu);
            Console.WriteLine("Création d'un milieu avec action Passe");

            Ailier ailier2 = new Ailier(9) { PlayingAction = new Dribble() }; 
            demo.AddPlayer(ailier2);
            Console.WriteLine("Création d'un ailier avec action dribble");

            Gardien gardien = new Gardien(1) { PlayingAction = new Dégagement() };
            demo.AddPlayer(gardien);
            Console.WriteLine("Création d'un gardien avec action dégagement");

            // Jouer le match
            demo.Jouer();
        }
    }
}
