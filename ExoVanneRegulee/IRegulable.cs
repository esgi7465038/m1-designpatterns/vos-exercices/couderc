﻿namespace Vannes;

public interface IRegulable
{
    public void Regule(int consigneDebit);
    public bool IsReguleOn { get; set; }
}
