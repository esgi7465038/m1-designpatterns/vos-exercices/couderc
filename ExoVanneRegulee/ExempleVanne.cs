﻿// See https://aka.ms/new-console-template for more information

using Vannes;

Vanne? MaVanne;			    // Déclaration de l'objet MaVanne (? => nullable...)
MaVanne = new Vanne(200);	// Instanciation de l'objet MaVanne

Console.Write("Ouvrir MaVanne: ");
MaVanne.Position = Convert.ToInt32(Console.ReadLine()); 	                                    // affectation avec set
Console.WriteLine("MaVanne est ouverte à {0}", MaVanne.Position);   // lecture avec get
Console.WriteLine("\nDescription de MaVanne: {0}", MaVanne);        //  Utilisation de ToString

if (MaVanne.Position > 75)
    MaVanne.Ouvrir(75);
Console.WriteLine("Débit: " + MaVanne.Debit);

MaVanne = null;

VanneRegulee MaVanneRegulee = new VanneRegulee();
MaVanneRegulee.IsReguleOn = true;
MaVanneRegulee.Regule(75);
Console.WriteLine("MaVanneRegulee est ouverte à {0}", MaVanneRegulee.Position);
Console.WriteLine("\nDescription de MaVanneRegulee: {0}", MaVanneRegulee);

