﻿using Etats;

Etudiant UnEtudiant = new Etudiant("Pal");

//  Etat Initial -> Heureux
Console.WriteLine(UnEtudiant.DireBonjour());

UnEtudiant.PartirEnVacance();
//  Pas de transition, reste Heureux
Console.WriteLine(UnEtudiant.DireBonjour());

UnEtudiant.RentrerDeVacance();
//  --> Contrarié
Console.WriteLine(UnEtudiant.DireBonjour());

try
{
    //  Lance une exception TransitionImpossibleException
    UnEtudiant.RentrerDeVacance();
    Console.WriteLine(UnEtudiant.DireBonjour());
}
catch (TransitionImpossibleException ex)
{
    Console.WriteLine("Exception Transition Impossible: " + ex.Message);
}
finally
{
    //  Toujours Dépité
    Console.WriteLine(UnEtudiant.DireBonjour());
}

UnEtudiant.Travailler(true);
// --> Depité
Console.WriteLine(UnEtudiant.DireBonjour());

