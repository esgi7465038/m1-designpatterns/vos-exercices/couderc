namespace Etats
{
    class EtatDepite : IEtatEtudiant
    {
        public string DireBonjour(Etudiant contexte)
        {
            return "Pfff Ggrrrrrr";
        }

        public void PartirEnVacance(Etudiant contexte)
        {
            Console.WriteLine("Youpi! J'étais Depité, je deviens Heureux");
            contexte.EtatCourant = contexte.LesEtats[Etudiant.etatHeureux];
        }

        public void RentrerDeVacance(Etudiant contexte)
        {
            throw new TransitionImpossibleException("Pff, J'suis dépité");
        }
        
        public void Travailler(Etudiant contexte, bool afaire)
        {
            if (afaire == false)
            {
                Console.WriteLine("Pff, J'suis déjà Contrarié...");
                contexte.EtatCourant = contexte.LesEtats[Etudiant.etatContrarie];
            }
            else
            {
                Console.WriteLine("Pff, Encore du travail...");
                contexte.EtatCourant = contexte.LesEtats[Etudiant.etatDepite];
            }
        }
    }
}