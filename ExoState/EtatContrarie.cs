﻿namespace Etats
{
    class EtatContrarie : IEtatEtudiant
    {
        public string DireBonjour(Etudiant contexte)
        {
            return "'jour...";
        }

        public void PartirEnVacance(Etudiant contexte)
        {
            Console.WriteLine("Youpi! J'étais Contrarié, je deviens Heureux");
            contexte.EtatCourant = contexte.LesEtats[Etudiant.etatHeureux];
        }

        public void RentrerDeVacance(Etudiant contexte)
        {
            Console.WriteLine("Pff, J'suis déjà Contrarié...");
        }
        
        public void Travailler(Etudiant contexte, bool afaire)
        {
            if (afaire == false)
            {
                Console.WriteLine("Pff, Pas de boulot, j'suis contrarié...");
                contexte.EtatCourant = contexte.LesEtats[Etudiant.etatContrarie];
            }
            else
            {
                Console.WriteLine("Pff, Encore du travail...");
                contexte.EtatCourant = contexte.LesEtats[Etudiant.etatDepite];
            }
        }
    }
}
