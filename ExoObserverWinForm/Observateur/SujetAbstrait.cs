﻿namespace Observateur
{
    public abstract class SujetAbstrait : ISujet
    {
        //  La liste des observateurs.
        private readonly List<IObservateur> observateurs = new List<IObservateur>();

        //  Le nombre d'observateur inscrits
        public int NbObservateur
        {
            get { return this.observateurs.Count; }
        }

        // Ajoute un observateur à la liste.
        public void AjouteObservateur(IObservateur observateur)
        {
            if (!observateurs.Contains(observateur))
                observateurs.Add(observateur);
        }

        // Enlève un observateur de la liste.
        public void RetireObservateur(IObservateur observateur)
        {
            if (observateurs.Contains(observateur))
                observateurs.Remove(observateur);
        }

        //  Notifie le changement à tous les observateurs
        public void NotifierObservateurs()
        {
            foreach (IObservateur observateur in observateurs)
                observateur.Actualiser(this);
        }
    }
}
