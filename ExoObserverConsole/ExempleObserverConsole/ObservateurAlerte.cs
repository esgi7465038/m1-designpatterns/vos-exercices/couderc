using ExerciceObserverConsole;
using Observateur;

namespace ExoObserverConsole
{
    internal class ObservateurAlerte : IObservateur
    {
        public void Actualiser(ISujet sujet)
        {
            var valObservee = ((SujetConcret)sujet).ValObservee;
            if (valObservee > 90)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ALERTE !!");
                Console.ResetColor();
            }
            else if (valObservee > 75)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Attention !");
                
            }
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"--> {GetHashCode()} Val obs: {valObservee}");
            Console.ResetColor();
        }
    }
}