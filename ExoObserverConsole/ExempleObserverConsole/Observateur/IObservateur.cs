﻿namespace Observateur
{
    public interface IObservateur
    {
        /// <summary>
        /// Méthode utilisée par la méthode NotifierObservateurs de ISujet.
        /// A implémenter dans les observateurs concrets.
        /// </summary>
        /// <param name="sujet">Le sujet à l'origiine de la notification</param>
        void Actualiser(ISujet sujet);
    }
}
